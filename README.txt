CombatBiplanes é um jogo de nave desenvolvido em Python utilizando Pygame que pode ser jogado em modo Cliente/Servidor.

Contributors:
    - Luciano Camargo Cruz    <luciano@lccruz.net>
    - Joao Toss Molon         <jtmolon@gmail.com>

License: 
    GPL

Requisitos:
    Python >= 2.6
    Pygame

Rodar App:
    Somente para teste:
        python main.py

    Rodar modo Cliente/Servidor:
        Rodar servidor:
            python server.py
        Rodar cliente:
            python main_communication.py
