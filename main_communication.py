# -*- coding: utf-8 -*-

from CombatBiplanes import *
from client import ClientSocket
import pygame
from time import sleep
from config import *

pygame.init()

def main():
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption(TITLE)

    background = pygame.Surface(screen.get_size())
    background.fill((0, 0, 0))

    font = pygame.font.SysFont("Liberation Serif", 30)

    cliente = ClientSocket()
    #chama a configuracao inicial do biplane do servidor
    cliente.configure()

    biplane = CombatBiplanes(cliente.cor, cliente.x, cliente.y, cliente.angulo, screen)
    shot = Shot(cliente.cor, cliente.x - (IMAGE_W / 2), cliente.y - (IMAGE_H / 2), screen)
    allcombatbiplanes = pygame.sprite.Group(biplane, shot)

    #busca a posicao dos inimigos ate todos terem se conectado
    while len(cliente.enemies) != cliente.players - 1:
        #busca a posicao dos inimigos no servidor
        cliente.enemiesPosition()

        #so desenha na tela caso ja tenha todos os inimigos
        if len(cliente.enemies) == cliente.players - 1:
            enemies = {}
            enemies_shots = {}
            for enemy in cliente.enemies:        
                enemies[enemy.cor] = CombatBiplanes(enemy.cor, enemy.x, enemy.y, enemy.angulo, screen)                
                enemies_shots[enemy.cor] = Shot(enemy.cor, enemy.x - (IMAGE_W / 2), enemy.y - (IMAGE_H / 2), screen)
        else:
            sleep(1)

    group_enemies = pygame.sprite.Group()
    group_enemies_shot = pygame.sprite.Group()
    for chave, enemy in enemies.items():
        allcombatbiplanes.add(enemy)
        group_enemies.add(enemy)
    for chave, enemy_shot in enemies_shots.items():
        allcombatbiplanes.add(enemy_shot)
        group_enemies_shot.add(enemy_shot)

    clock = pygame.time.Clock()
    keepGoing = True
    while keepGoing:
        # taxa do frame
        clock.tick(30)
        # eventos
        cliente.currentLife()
        if not cliente.vida:
            keepGoing = False
            cliente.died()
            print "MORRI"
            continue
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                keepGoing = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    biplane.turnLeft()
                    cliente.rotate('esquerda')
                elif event.key == pygame.K_RIGHT:
                    biplane.turnRight()
                    cliente.rotate('direita')
                elif event.key == pygame.K_SPACE and not shot.speed:
                    shot.speed = SPEEDSHOT
                    shot.ang = biplane.ang
                    shot.set_x_y(biplane.rect.centerx, biplane.rect.centery)
                    cliente.shoot(shot.speed)
        if not shot.speed:
            shot.set_x_y(biplane.rect.centerx, biplane.rect.centery)
            cliente.shoot(0)

        #enemies position atualiza a posicao dos inimigos na tela
        cliente.enemiesPosition()
        if not len(cliente.enemies):
            keepGoing = False
            print "VENCI!"

        active_enemies = []
        dic_labels = {}
        dic_labels[cliente.cor] = font.render("%s" % (cliente.vida), 1, DIC_COLORS[cliente.cor][0])
        for enemy in cliente.enemies:
            active_enemies.append(enemy.cor)
            #enemies eh a lista dos sprites dos inimigos
            sprite_biplane = enemies[enemy.cor]
            #cliente.enemies eh a lista de inimigos que vem do servidor
            sprite_biplane.ang = enemy.angulo

            #atualizar posicao do shot dos inimigos
            sprite_shot = enemies_shots[enemy.cor]
            sprite_shot.speed = enemy.atirando
            if not sprite_shot.speed:
                sprite_shot.set_x_y(sprite_biplane.rect.centerx, sprite_biplane.rect.centery)
                sprite_shot.ang = enemy.angulo
            dic_labels[enemy.cor] = font.render("%s" % (enemy.vida), 1, DIC_COLORS[enemy.cor][0])

        #se a cor de um inimigo nao constar mais na lista de ativos, remove ele da tela
        for key in enemies.keys():
            if key not in active_enemies:
                allcombatbiplanes.remove(enemies.pop(key))
                allcombatbiplanes.remove(enemies_shots.pop(key))

        screen.fill((0,0,0))
        allcombatbiplanes.clear(screen, background)
        allcombatbiplanes.update()

        #testa se eu atingi alguem
        if shot.speed:
            list_collide = pygame.sprite.spritecollide(shot, group_enemies, False)
            for enemy_collide in list_collide:
                shot.speed = 0
                cliente.shoot(0)
                cliente.hit(enemy_collide.cor)

        #desenha no screen
        allcombatbiplanes.draw(screen)
        for key, valor in dic_labels.items():
            screen.blit(valor, DIC_COLORS[key][1])
        pygame.display.flip()

    cliente.sock.close()

main()
