# -*- coding: utf-8 -*-

from CombatBiplanes import *
import pygame
from config import *

pygame.init()

def main():
    screen = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption(TITLE)

    background = pygame.Surface(screen.get_size())
    background.fill((0, 0, 0))

    biplane = CombatBiplanes('BLUE',40, 40, 0,screen)
    shot = Shot('BLUE',0,0, screen)
    biplanes = pygame.sprite.Group(biplane)
    allsprites = pygame.sprite.RenderPlain(biplane,shot)

    yellow = (255, 255, 0)
    myfont = pygame.font.SysFont("Liberation Serif", 30)
    label = myfont.render("Python and Pygame are Fun!", 1, yellow)

    clock = pygame.time.Clock()
    keepGoing = True
    while keepGoing:
        # taxa do frame
        clock.tick(30)
        # eventos
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                keepGoing = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    biplane.turnLeft()
                elif event.key == pygame.K_RIGHT:
                    biplane.turnRight()
                elif event.key == pygame.K_SPACE and not shot.speed:
                    shot.speed = 15
                    shot.ang = biplane.ang
                    shot.set_x_y(biplane.rect.centerx, biplane.rect.centery)
        if not shot.speed:
            shot.set_x_y(biplane.rect.centerx, biplane.rect.centery)

        allsprites.clear(screen, background)
        allsprites.update()
        #desenha no screen
        allsprites.draw(screen)
        screen.blit(label, (100, 100))
        pygame.display.flip()

main()
