# -*- coding: utf-8 -*-

import socket
import threading
import SocketServer
import random
from config import *

COLORS = {'BLUE' : False, 
          'RED' : False, 
          'WHITE' : False, 
          'YELLOW' : False}

ACTIVE_PLAYERS = {}

class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):

    PLAYERS = 2

    def processMessage(self, message):
        """Process the message according to the type."""
        data = eval(message)
        messageType = data['type']

        retorno = {}

        #type == 0 -> configuracao inicial
        if messageType == 0:
            for key in COLORS.keys():
                if not COLORS[key]:
                    COLORS[key] = True
                    
                    retorno['players'] = self.PLAYERS
                    retorno['cor'] = key
                    retorno['x'] = random.randrange(IMAGE_W / 2 , MAX_X - (IMAGE_W / 2))
                    retorno['y'] = random.randrange(IMAGE_H / 2 , MAX_Y - (IMAGE_H / 2))

                    ACTIVE_PLAYERS[key] = dict(cor=retorno['cor'], x=retorno['x'], y=retorno['y'], angulo=0, atirando=0, vida=3, thread=self.request)

                    break

        #type == 1 -> posicao dos outros jogadores
        elif messageType == 1:
            retorno['enemies'] = []
            #cor do biplane que solicitou a posicao dos outros jogadores
            cor = data['cor']
            idx = 1
            for key, value in ACTIVE_PLAYERS.items():
                #se nao for o proprio cara, atualiza a posicao
                #if key != cor:
                retorno['enemies'].append({'player'+str(idx)+'cor': value['cor'],
                                           'player'+str(idx)+'x': value['x'],
                                           'player'+str(idx)+'y': value['y'],
                                           'player'+str(idx)+'angulo': value['angulo'],
                                           'player'+str(idx)+'atirando': value['atirando'],
                                           'player'+str(idx)+'vida': value['vida']})
                idx += 1

        #type == 2 -> jogador vai se mover
        elif messageType == 2:
            if data['sentido'] == 'direita':
                variacao = -45
            else:
                variacao = 45
            cor = data['cor']
            if cor in ACTIVE_PLAYERS.keys():
                angulo = ACTIVE_PLAYERS[cor]['angulo'] + variacao
                if angulo == 360:
                    angulo = 0 
                elif angulo < 0:
                    angulo = 315
                ACTIVE_PLAYERS[cor]['angulo'] = angulo

            retorno['retorno'] = 'OK'

        #type == 3 -> jogador atira
        elif messageType == 3:
            cor = data['cor']
            if cor in ACTIVE_PLAYERS.keys():
                ACTIVE_PLAYERS[cor]['atirando'] = data['speed']
            retorno['retorno'] = 'ATIREI'

        #type == 4 -> jogador foi foi atingido
        elif messageType == 4:
            cor = data['cor']
            if cor in ACTIVE_PLAYERS.keys():
                ACTIVE_PLAYERS[cor]['vida'] -= 1
            retorno['retorno'] = 'ATINGI'

        #type == 5 -> jogador consulta a sua vida
        elif messageType == 5:
            cor = data['cor']
            vida_restante = 0
            if cor in ACTIVE_PLAYERS.keys():
                vida_restante = ACTIVE_PLAYERS[cor]['vida']
            retorno['vida'] = vida_restante

        #type == 6 -> jogador informa que morreu
        elif messageType == 6:
            cor = data['cor']
            #jogador eh retirado dos ACTIVE_PLAYERS
            if cor in ACTIVE_PLAYERS.keys():
                ACTIVE_PLAYERS.pop(cor)
            retorno['retorno'] = 'MORRI'

        retorno = str(retorno)
        self.request.sendall(retorno)

    def handle(self):
        while True:
            message = self.request.recv(1024)
            if message:
                self.processMessage(message)

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

def main():

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    ip, port = server.server_address

    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()

    server.RequestHandlerClass.PLAYERS = int(raw_input("Informe o número de jogadores (2 - 4):"))

    print "Servidor rodando na thread:", server_thread.name

    while True:
        pass
        
main()
