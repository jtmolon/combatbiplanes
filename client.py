# -*- coding: utf-8 -*-

import socket
import sys
from config import *

class EnemyBiplane(object):

    def __init__(self):
        """
        """
        self.cor = ""
        self.x = 0
        self.y = 0
        self.angulo = 0
        #atirando comeca com 0, e muda pra 15 em caso de tiro
        self.atirando = 0
        self.vida = 3
    

class ClientSocket(object):

    def __init__(self):
        """
        """
        self.cor = ""
        self.x = 0
        self.y = 0
        self.vida = 3
        self.angulo = 0
        self.enemies = []
        self.players = 2
        # Create a socket (SOCK_STREAM means a TCP socket)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Connect to server
        self.sock.connect((HOST, PORT))

    def configure(self):
        """Conecta no servidor para obter a configuracao inicial
        """
        message = {}
        # 0 = conexao inicial, pega cor e posicao
        message['type'] = 0
        data = str(message)
        self.sock.sendall(data)

        # Receive data from the server and shut down
        received = self.sock.recv(1024)
        
        retorno = eval(received)
        self.cor = retorno['cor']
        self.x = retorno['x']
        self.y = retorno['y']
        self.players = retorno['players']

    def enemiesPosition(self):
        """Pede para o servidor onde estao os outros jogadores
        """
        message = {}
        message['type'] = 1
        message['cor'] = self.cor
        data = str(message)
        self.sock.sendall(data)

        received = self.sock.recv(1024)

        retorno = eval(received)
        idx = 1
        self.enemies = []
        for player in retorno['enemies']:
            if self.cor != player['player'+str(idx)+'cor']:
                enemy = EnemyBiplane()
                enemy.cor = player['player'+str(idx)+'cor']
                enemy.x = player['player'+str(idx)+'x']
                enemy.y = player['player'+str(idx)+'y']
                enemy.angulo = player['player'+str(idx)+'angulo']
                enemy.atirando = player['player'+str(idx)+'atirando']
                enemy.vida = player['player'+str(idx)+'vida']
                self.enemies.append(enemy)
            idx += 1

    def rotate(self, sentido):
        """Avisa o servidor que vai rotacionar, para direita ou esquerda
        """
        message = {}
        message['type'] = 2
        message['cor'] = self.cor
        message['sentido'] = sentido
        
        data = str(message)
        self.sock.sendall(data)

        received = self.sock.recv(1024)

    def shoot(self, speed):
        """Avisa o servidor se esta atirando
        """
        message = {}
        message['type'] = 3
        message['cor'] = self.cor
        message['speed'] = speed

        data = str(message)
        self.sock.sendall(data)

        received = self.sock.recv(1024)

    def hit(self, cor):
        """Avisa o servidor que atingiu alguem
        """
        message = {}
        message['type'] = 4
        message['cor'] = cor

        data = str(message)
        self.sock.sendall(data)

        received = self.sock.recv(1024)

    def currentLife(self):
        """Consulta sua vida no servidor
        """
        message = {}
        message['type'] = 5
        message['cor'] = self.cor

        data = str(message)
        self.sock.sendall(data)

        received = self.sock.recv(1024)

        retorno = eval(received)
        self.vida = retorno['vida']

    def died(self):
        """Informa ao servidor que morreu
        """
        message = {}
        message['type'] = 6
        message['cor'] = self.cor

        data = str(message)
        self.sock.sendall(data)
        
        received = self.sock.recv(1024)
