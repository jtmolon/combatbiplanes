# -*- coding: utf-8 -*-

#Communication
HOST, PORT = "localhost", 8081

#Server
MAX_X = 800
MAX_Y = 600 
IMAGE_W = 50
IMAGE_H = 30

#CombatBiplanes.py
SPEEDBIPLANE = 1
SPEEDSHOT = 10

#Screen
WIDTH = 800
HEIGHT = 600

#Game
TITLE = "Combat Biplanes"
VIDA = 3

#COLORS
DIC_COLORS = {'BLUE': ((0, 0, 255), (100,10)),
              'RED': ((255, 0, 0), (400, 10)),
              'WHITE': ((255, 255, 255), (200, 10)),
              'YELLOW': ((255, 255, 0), (300, 10))}
