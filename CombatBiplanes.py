# -*- coding: utf-8 -*-

import pygame
from config import *

def calcPos(obj):
    """ 
        Testa o angulo da imagem e atribui valores para Dx/Dy.
        Multiplica Dx/Dy com speed, isso faz a imagem se mover no mesmo sentido atual.
    """
    if obj.ang == 0:
        obj.dx = 1
        obj.dy = 0
    elif obj.ang == 45:
        obj.dx = 0.5
        obj.dy = -0.5
    elif obj.ang == 90:
        obj.dx = 0
        obj.dy = -1
    elif obj.ang == 135:
        obj.dx = -0.5
        obj.dy = -0.5
    elif obj.ang == 180:
        obj.dx = -1
        obj.dy = 0
    elif obj.ang == 225:
        obj.dx = -0.5
        obj.dy = 0.5
    elif obj.ang == 270:
        obj.dx = 0
        obj.dy = 1
    elif obj.ang == 315:
        obj.dx = 0.5
        obj.dy = 0.5
    obj.dx *= obj.speed
    obj.dy *= obj.speed

class Shot(pygame.sprite.Sprite):
    def __init__(self, cor, x, y, screen):
        pygame.sprite.Sprite.__init__(self)    
        self.imageMaster = pygame.image.load("imagens/shot_%s.png" % (cor))
        self.imageMaster = self.imageMaster.convert()
        self.image = self.imageMaster
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.x = self.rect.centerx
        self.y = self.rect.centery
        #angulo
        self.ang = 0
        #x/y temporario
        self.dx = 0
        self.dy = 0 
        #velocidade de movimentacao
        # speed = 0 == imagem parada
        self.speed = 0
        self.screen = screen

    def update(self):
        self.rect.centerx = self.x
        self.rect.centery = self.y
        calcPos(self)
        self.x += self.dx
        self.y += self.dy
        self.rect.centerx = self.x
        self.rect.centery = self.y
        #testa colisao com os limites da tela
        if not self.screen.get_rect().collidepoint(self.x, self.y):
            self.speed = 0

    def set_x_y(self, x, y):
        self.x = x
        self.y = y

class CombatBiplanes(pygame.sprite.Sprite):
    def __init__(self, cor, x, y, ang, screen):
        pygame.sprite.Sprite.__init__(self)    
        self.imageMaster = pygame.image.load("imagens/air_plane_%s.png" % (cor))
        self.imageMaster = self.imageMaster.convert()
        self.image = self.imageMaster
        #get the rectangular area of the Surface
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.x = self.rect.centerx
        self.y = self.rect.centery
        #angulo
        self.ang = ang
        #x/y temporario
        self.dx = 0
        self.dy = 0 
        #velocidade de movimentacao
        self.speed = SPEEDBIPLANE
        self.cor = cor
        self.screen = screen

    def update(self):
        """Atualiza a posicao da imagem no screen
        """
        oldCenter = self.rect.center

        oldImage = self.image
        oldRect = self.rect
        oldX = self.x
        oldY = self.y

        self.image = pygame.transform.rotate(self.imageMaster, self.ang)
        self.rect = self.image.get_rect()
        self.rect.center = oldCenter

        calcPos(self)
        self.x += self.dx
        self.y += self.dy
        self.rect.centerx = self.x
        self.rect.centery = self.y
        
        self.image_w, self.image_h = self.image.get_size()
        bounds_rect = self.screen.get_rect().inflate(
                        -self.image_w, -self.image_h)

        collide = self.x < bounds_rect.left or self.x > bounds_rect.right or self.y < bounds_rect.top or self.y > bounds_rect.bottom

        #caso colida, retorna a posicao anterior
        if collide:
            self.image = oldImage
            self.rect = oldRect
            self.x = oldX
            self.y = oldY

    def turnLeft(self):
        """Rotaciona a imagem para esquerda
        """
        self.ang += 45
        if self.ang == 360:
            self.ang = 0

    def turnRight(self):
        """Rotaciona a imagem para direita
        """
        self.ang -= 45
        if self.ang < 0:
            self.ang = 315
